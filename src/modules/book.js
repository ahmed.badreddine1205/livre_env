"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
var mongoose_paginate_1 = require("mongoose-paginate");
var bookSchema = new mongoose_1["default"].Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    price: { type: Number, requied: true },
    publishingDate: { type: Date, required: true, "default": new Date() },
    available: { type: Boolean, requied: true, "default": true },
    quantity: { type: Number, required: true, "default": 0 }
});
bookSchema.plugin(mongoose_paginate_1["default"]);
var Book = mongoose_1["default"].model("Book", bookSchema);
exports["default"] = Book;
