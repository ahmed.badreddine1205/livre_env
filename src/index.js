"use strict";
exports.__esModule = true;
var express_1 = require("express");
var book_1 = require("./modules/book");
var body_parser_1 = require("body-parser");
var serve_static_1 = require("serve-static");
var mongoose_1 = require("mongoose");
var cors_1 = require("cors");
/* Instancier Express */
var app = express_1["default"]();
/* Middleware bodyParser pour parser le corps des requêtes en Json*/
app.use(body_parser_1["default"].json());
/* Middlware pour configurer le dossier des ressources statique*/
app.use(serve_static_1["default"]("public"));
/* Actvier CORS*/
app.use(cors_1["default"]());
/* Connection à MongoDb*/
var uri = "mongodb://localhost:27017/biblio";
mongoose_1["default"].connect(uri, function (err) {
    if (err) {
        console.log(err);
    }
    else {
        console.log("Mongo db connection sucess");
    }
});
app.get("/", function (req, resp) {
    resp.send("Hello world");
});
/* Requête HTTP GET http://localhost:8700/ */
app.get("/", function (req, resp) {
    resp.send("Hello world");
});
/* Requête HTTP GET http://localhost:8700/books */
app.get("/books", function (req, resp) {
    book_1["default"].find(function (err, books) {
        if (err) {
            resp.status(500).send(err);
        }
        else {
            resp.send(books);
        }
    });
});
/* Requête HTTP GET http://localhost:8700/books/id */
app.get("/books/:id", function (req, resp) {
    book_1["default"].findById(req.params.id, function (err, book) {
        if (err) {
            resp.status(500).send(err);
        }
        else {
            resp.send(book);
        }
    });
});
/* Requête HTTP POST http://localhost:8700/books */
app.post("/books", function (req, resp) {
    var book = new book_1["default"](req.body);
    book.save(function (err) {
        if (err)
            resp.status(500).send(err);
        else
            resp.send(book);
    });
});
/* Requête HTTP PUT http://localhost:8700/books/id */
app.put("/books/:id", function (req, resp) {
    book_1["default"].findByIdAndUpdate(req.params.id, req.body, function (err, book) {
        if (err)
            resp.status(500).send(err);
        else {
            resp.send("Successfuly updated book");
        }
    });
});
/* Requête HTTP DELETE http://localhost:8700/books/id */
app["delete"]("/books/:id", function (req, resp) {
    book_1["default"].deleteOne({ _id: req.params.id }, function (err) {
        if (err)
            resp.status(500).send(err);
        else
            resp.send("Successfuly deleted Book");
    });
});
app.get("/pbooks", function (req, resp) {
    var p = parseInt(req.query.page || '1');
    var size = parseInt(req.query.size || '5');
    book_1["default"].paginate({}, { page: p, limit: size }, function (err, result) {
        if (err)
            resp.status(500).send(err);
        else
            resp.send(result);
    });
});
app.get("/books-serach", function (req, resp) {
    var p = parseInt(req.query.page || '1');
    var size = parseInt(req.query.size || '5');
    var keyword = req.query.kw || '';
    book_1["default"].paginate({ title: { $regex: ".*(?i)" + keyword + ".*" } }, { page: p, limit: size }, function (err, result) {
        if (err)
            resp.status(500).send(err);
        else
            resp.send(result);
    });
});

/* Démarrer le serveur*/
app.listen(8700, function () {
    console.log("Server Started on port %d", 8700);
});
